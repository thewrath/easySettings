# EasySettings

## What is this ? 
As its name suggests, EasySettings is a module that allows you to manage the parameters of a simple application (Java application).

Via annotations you can mark class variables so that they are taken into account by the parameter management system.

Taking into account by the system means that this variable will be accessible in different sources of settings of the application: 
- LibGDX setting window
- A configuration file in different formats :
    - Json
    - .properties
    - Xml
    - ...

This library aims to be light and easily extendable. You can define your own parameter sources (external database, ...)

## How to use ?

The first step is to annotate the instance variables that you want to transform into parameters:

```java

public class Main {

    @Setting(name = "name", section = "main", type = StringSetting.class)
    public static String name = "test";

    @Setting(name = "age", section = "main", type = IntegerSetting.class)
    public static Integer age = 28;
}
```

The `Setting` annotation takes as attributes the name of the setting, the section (category) and the type of the setting.

For the moment only the following primitive types can be used:
- IntegerSetting
- StringSetting

Then you have to create a `SettingsContainer` this class contains all the settings.
Factories allow you to easily build this object.

```java
SettingsContainerMetadataFactory scmf = new ClassesSettingsContainerMetadataFactory(Collections.singletonList(Main.class));
SettingsContainer sc = SettingsContainerFactory.createSettingsContainer(scmf.createMetadata());
```

Finally, you can define parameter sources that allow you to persist settings in file and others:

```java
SettingsSource settingsSource = new PropertiesSettingsSource("test.properties", sc);
settingsSource.persist();
```

## How to extends ?
- Définir des types de paramètre
- Définir une sources de paramètre
Todo