import fr.thewrath.easysettings.SettingsContainer;
import fr.thewrath.easysettings.SettingsContainerFactory;
import fr.thewrath.easysettings.annotation.Setting;
import fr.thewrath.easysettings.metadata.ClassesSettingsContainerMetadataFactory;
import fr.thewrath.easysettings.metadata.SettingsContainerMetadataFactory;
import fr.thewrath.easysettings.setting.IntegerSetting;
import fr.thewrath.easysettings.setting.StringSetting;
import fr.thewrath.easysettings.source.PropertiesSettingsSource;
import fr.thewrath.easysettings.source.SettingsSource;

import java.util.Collections;

public class Main {

    @Setting(name = "name", section = "main", type = StringSetting.class)
    public static String name = "test";

    @Setting(name = "age", section = "main", type = IntegerSetting.class)
    public static Integer age = 28;

    /**
     * Program entry point for test purpose.
     */
    public static void main(String[] args) throws Exception {
        System.out.println("Create settings container");
        SettingsContainerMetadataFactory settingsContainerMetadataFactory = new ClassesSettingsContainerMetadataFactory(Collections.singletonList(Main.class));
        SettingsContainer settingsContainer = SettingsContainerFactory.createSettingsContainer(settingsContainerMetadataFactory.createMetadata());

        settingsContainer.getSettings().forEach(setting -> System.out.println(setting.getName()));

        SettingsSource settingsSource = new PropertiesSettingsSource("test.properties", settingsContainer);
        settingsSource.persist();
    }
}
