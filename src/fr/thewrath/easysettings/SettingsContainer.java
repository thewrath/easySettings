package fr.thewrath.easysettings;

import fr.thewrath.easysettings.setting.Setting;

import java.util.ArrayList;
import java.util.List;

public class SettingsContainer {

    private final List<Setting<?>> settings;

    public SettingsContainer() {
        this.settings = new ArrayList<>();
    }

    public List<Setting<?>> getSettings() {
        return settings;
    }

    public void addSetting(Setting<?> setting) {
        this.settings.add(setting);
    }
}
