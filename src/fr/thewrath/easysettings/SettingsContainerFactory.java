package fr.thewrath.easysettings;

import fr.thewrath.easysettings.metadata.SettingMetadata;
import fr.thewrath.easysettings.metadata.SettingsContainerMetadata;

public class SettingsContainerFactory {

    /**
     * Create new SettingsContainer based on SettingsContainerMetadata
     *
     * @param settingsContainerMetadata
     * @return
     */
    public static SettingsContainer createSettingsContainer(SettingsContainerMetadata settingsContainerMetadata) throws Exception {
        SettingsContainer settingsContainer = new SettingsContainer();
        for (SettingMetadata settingMetadata : settingsContainerMetadata.getSettingsMetada()) {
            settingsContainer.addSetting(settingMetadata.createSetting());
        }

        return settingsContainer;
    }
}
