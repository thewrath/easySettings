package fr.thewrath.easysettings.source;

import fr.thewrath.easysettings.exception.SettingPersistenceException;

/**
 * Define Settings source, a source is used to persist the parameters (in a file, on a database etc).
 */
public interface SettingsSource {

    /**
     * Persist settings on the medium.
     */
    void persist() throws SettingPersistenceException;

    /**
     * Load settings from the medium and recover SettingsContainer instance.
     */
    void load();
}