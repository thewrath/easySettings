package fr.thewrath.easysettings.source;

import fr.thewrath.easysettings.SettingsContainer;
import fr.thewrath.easysettings.exception.SettingPersistenceException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 *  This class allows to manage the parameters via a source file in .properties format.
 */
public class PropertiesSettingsSource implements SettingsSource {

    private final String filePath;

    private final SettingsContainer settingsContainer;

    /**
     * Builds a new instance based on a file.
     * @param filePath the source file.
     */
    public PropertiesSettingsSource(String filePath, SettingsContainer settingsContainer)  {
        this.filePath = filePath;
        this.settingsContainer = settingsContainer;
    }

    @Override
    public void persist() throws SettingPersistenceException {
        try {
            FileWriter fileWriter = new FileWriter(filePath);
            Properties properties = new Properties();
            settingsContainer.getSettings().forEach(setting -> {
                if (setting.getValue() != null) {
                    properties.setProperty(setting.getName(), setting.getValue().toString());
                }
            });
            properties.store(fileWriter, "");
        } catch (IOException e) {
            throw new SettingPersistenceException("Error when creating or retrieving the persistence file.");
        }
    }

    @Override
    public void load() {

    }

    public String getFilePath() {
        return filePath;
    }

    public SettingsContainer getSettingsContainer() {
        return settingsContainer;
    }
}
