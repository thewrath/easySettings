package fr.thewrath.easysettings.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to bind method to setting.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Setting {

    /**
     * Name of the setting.
     */
    String name();

    /**
     * Section of the setting.
     */
    String section();

    /**
     * Type of the setting.
     */
    Class<? extends fr.thewrath.easysettings.setting.Setting<?>> type();
}
