package fr.thewrath.easysettings.metadata;

import fr.thewrath.easysettings.setting.Setting;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class SettingMetadata {

    private Field field;

    private fr.thewrath.easysettings.annotation.Setting settingAnnotation;

    private Class<? extends Setting<?>> settingClass;

    private String section;

    private String name;

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public fr.thewrath.easysettings.annotation.Setting getSettingAnnotation() {
        return settingAnnotation;
    }

    public void setSettingAnnotation(fr.thewrath.easysettings.annotation.Setting settingAnnotation) {
        this.settingAnnotation = settingAnnotation;
    }

    public Class<? extends Setting<?>> getSettingClass() {
        return settingClass;
    }

    public void setSettingClass(Class<? extends Setting<?>> settingClass) {
        this.settingClass = settingClass;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Create new Setting object from metadata.
     *
     * @return setting from metadata.
     * @throws Exception : Exception thrown if setting cannot be construct due to reflection exception.
     */
    public fr.thewrath.easysettings.setting.Setting<?> createSetting() throws Exception {
        try {
            fr.thewrath.easysettings.setting.Setting<?> setting = settingClass.getDeclaredConstructor().newInstance();
            setting.setName(settingAnnotation.name());

            Object value = this.field.get(null);
            if (value != null && value.getClass() == setting.getType()) {
                setting.setValue(setting.getType().cast(value));
            }
            return setting;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            // Fixme : Create specific exception.
            throw new Exception();
        }
    }
}
