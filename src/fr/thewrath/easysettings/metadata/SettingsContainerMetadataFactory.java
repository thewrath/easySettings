package fr.thewrath.easysettings.metadata;

public interface SettingsContainerMetadataFactory {

    /**
     *
     * @return
     */
    SettingsContainerMetadata createMetadata();
}
