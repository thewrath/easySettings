package fr.thewrath.easysettings.metadata;

import java.util.ArrayList;
import java.util.List;

public class SettingsContainerMetadata {

    private final List<SettingMetadata> settingsMetada;

    public SettingsContainerMetadata() {
        this.settingsMetada = new ArrayList<>();
    }

    public List<SettingMetadata> getSettingsMetada() {
        return settingsMetada;
    }

    public void addSettingMetadata(SettingMetadata settingMetadata) {
        this.settingsMetada.add(settingMetadata);
    }
}
