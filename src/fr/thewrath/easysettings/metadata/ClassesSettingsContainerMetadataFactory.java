package fr.thewrath.easysettings.metadata;

import fr.thewrath.easysettings.setting.Setting;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Builds the SettingMetadata object based on annotation in classes.
 */
public class ClassesSettingsContainerMetadataFactory implements SettingsContainerMetadataFactory {

    private final List<? extends Class<?>> classes;

    public ClassesSettingsContainerMetadataFactory(List<? extends Class<?>> classes) {
        this.classes = classes;
    }

    @Override
    public SettingsContainerMetadata createMetadata() {
        SettingsContainerMetadata settingsContainerMetadata = new SettingsContainerMetadata();

        // Fetch setting section
        classes.forEach(clazz -> {
            for (Field field : clazz.getFields()) {
                for (fr.thewrath.easysettings.annotation.Setting settingAnnotation : field.getDeclaredAnnotationsByType(fr.thewrath.easysettings.annotation.Setting.class)) {
                    SettingMetadata settingMetadata = new SettingMetadata();

                    settingMetadata.setSettingAnnotation(settingAnnotation);
                    settingMetadata.setSettingClass(settingAnnotation.type());
                    settingMetadata.setField(field);
                    settingMetadata.setName(settingAnnotation.name());
                    settingMetadata.setSection(settingAnnotation.section());

                    settingsContainerMetadata.addSettingMetadata(settingMetadata);
                }
            }
        });

        return settingsContainerMetadata;

    }
}
