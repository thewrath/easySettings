package fr.thewrath.easysettings.setting;

/**
 * Simple int parameter.
 */
public class IntegerSetting extends AbstractSetting<Integer> {

    @Override
    public Class<?> getType() {
        return Integer.class;
    }
}
