package fr.thewrath.easysettings.setting;

import fr.thewrath.easysettings.exception.IncompatibleSettingTypeException;

public abstract class AbstractSetting<Type> implements Setting<Type> {

    private Type value;

    private String name;

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    @Override
    public Type getValue() {
        return this.value;
    }

    @Override
    public void setValue(Object value) {
        if (value.getClass() != getType()) throw new IncompatibleSettingTypeException();
        this.value = (Type) value;
    }
}
