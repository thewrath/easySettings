package fr.thewrath.easysettings.setting;

public class StringSetting extends AbstractSetting<String> {

    @Override
    public Class<?> getType() {
        return String.class;
    }
}
