package fr.thewrath.easysettings.setting;

import fr.thewrath.easysettings.exception.IncompatibleSettingTypeException;

/**
 * Interface representing a setting.
 * @param <Type>
 */
public interface Setting<Type> {

    /**
     *
     * @param name
     */
    public void setName(String name);

    /**
     *
     * @return
     */
    public String getName();

    /**
     * Retrieves the parameter value.
     */
    public Type getValue();

    /**
     * Set parameter value.
     * @param value
     */
    public void setValue(Object value) throws IncompatibleSettingTypeException;

    /**
     * Return the type of the paramter.
     */
    public Class<?> getType();
}
