package fr.thewrath.easysettings.exception;

public class SettingPersistenceException extends Exception {

    private final String reason;

    public SettingPersistenceException(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}
